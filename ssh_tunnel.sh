#!/bin/bash

keyfile=~/.ssh/server
serverUser=user
serverIP=25.25.25.25

createTunnel() {
	ssh -N -R 2222:localhost:22 $serverUser@$serverIP -i $keyfile
	if [[ $? -eq 0 ]]; then
		echo Tunnel to jumpbox created successfully
	else
		echo An error occurred creating a tunnel to jumpbox. RC was $?
	fi
}
ps -aux | grep $serverIP | grep -v grep
if [[ $? -ne 0 ]]; then
	echo Creating new tunnel connection
	createTunnel
fi

